# Auth2

Microservicio para el manejo de permisos para roles y/o usuarios4

## Descripción

El microservicio Auth2 es el encargado de gestionar los permisos de los roles, asi como tambien de los usuarios, esto quiere decir que los permisos creados pueden ser asignados a un rol (lo que significa que todo usuario que tenga ese rol poseera el permiso), a un usuario (solo los usuarios seleccionados solo tendran el permiso) o a ambos (todos los usuarios seleccionados mas los usuarios que tienen el/los roles seleccionados tendran el permiso).

## Requisitos

Para poder ver la librería se necesitará tener instalado Git para poder clonarse el proyecto.

### Desarrollo

* [Java JDK 11](https://www.oracle.com/ar/java/technologies/javase-jdk11-downloads.html) o superior
* [Docker](https://docs.docker.com/)

### Configuración

Es necesario configurar las siguientes variables de entorno

```dotenv
AUTH2_PORT=8080
AUTH2_URL=/auth2/v1
MYSQL_IP=localhost
MYSQL_PORT=3306
AUTH2_DB_NAME=auth2
AUTH2_USER=starkloff
AUTH2_PASS=starkloff
URL_AUTH=http://localhost:3000/v1
LOG_LEVEL=debug
M2_CONFIG={{path_to_.m2}}(opcional)
```

## APIs Auth2

[Documentación de API](./README-API.md)

## Modelo de datos

[Modelo de datos](https://gitlab.com/IStarkloff/auth2/-/blob/main/Documentacion/modelo%20de%20datos.png)

## Levantar Proyecto

Definir archivo .env en la misma ruta en donde se encuentra ubicado el archivo docker-compose.yaml y pegar lo siguiente

```dotenv
AUTH2_PORT=8080
AUTH2_URL=/auth2/v1
MYSQL_IP=localhost
MYSQL_PORT=3306
AUTH2_DB_NAME=auth2
AUTH2_USER=starkloff
AUTH2_PASS=starkloff
URL_AUTH=http://localhost:3000/v1
LOG_LEVEL=debug
M2_CONFIG={{path_to_.m2}}(opcional)
```

Ejecutar el comando:

```bash
docker-compose up
```

## Autores
Ignacio Starkloff




