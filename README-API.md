# Auth2 API

Auth2 API

## Indices

* [Permission](#permission)

  * [DELETE permission/{id}](#1-delete-permission{id})
  * [GET permission](#2-get-permission)
  * [GET permission/all](#3-get-permissionall)
  * [GET permission/name/{name}](#4-get-permissionname{name})
  * [GET permission/{id}](#5-get-permission{id})
  * [POST permission](#6-post-permission)
  * [POST permission/validate](#7-post-permissionvalidate)
  * [PUT permission](#8-put-permission)

* [Privileged](#privileged)

  * [DELETE privileged/{id}](#1-delete-privileged{id})
  * [GET privileged](#2-get-privileged)
  * [GET privileged/all](#3-get-privilegedall)
  * [GET privileged/external_id/{id}](#4-get-privilegedexternal_id{id})
  * [GET privileged/{id}](#5-get-privileged{id})
  * [POST privileged](#6-post-privileged)
  * [PUT privileged](#7-put-privileged)
  * [PUT privileged/assign_permission](#8-put-privilegedassign_permission)
  * [PUT privileged/unassign_permission](#9-put-privilegedunassign_permission)


--------


## Permission



### 1. DELETE permission/{id}



***Endpoint:***

```bash
Method: DELETE
Type: 
URL: {{urlauth2}}/permission/:id
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Authorization | {{token}} |  |



***URL variables:***

| Key | Value | Description |
| --- | ------|-------------|
| id | 1 | (Required)  |



#### Response


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Authorization | {{token}} | (Required)  |



***Query:***

| Key | Value | Description |
| --- | ------|-------------|
| id | <int64> | (Required)  |



***Status Code:*** 200

<br>



### 2. GET permission



***Endpoint:***

```bash
Method: GET
Type: 
URL: {{urlauth2}}/permission
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Authorization | {{token}} |  |



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| size | 3 |  |
| page | 0 |  |



#### Response


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Authorization | {{token}} | (Required)  |

***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| size | 3 |  |
| page | 0 |  |


***Body:***

```json
{
    "total": 3,
    "page": 1,
    "items": [
        {
            "id": 1,
            "deletedDate": null,
            "createdDate": "2021-11-22T02:33:39.000+00:00",
            "name": "Test2"
        },
        {
            "id": 2,
            "deletedDate": null,
            "createdDate": "2021-11-22T04:13:26.000+00:00",
            "name": "Test3"
        },
        {
            "id": 3,
            "deletedDate": null,
            "createdDate": "2021-11-22T04:52:19.000+00:00",
            "name": "Test4"
        }
    ]
}
```



***Status Code:*** 200

<br>



### 3. GET permission/all



***Endpoint:***

```bash
Method: GET
Type: 
URL: {{urlauth2}}/permission/all
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Authorization | {{token}} |  |



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| size | 3 |  |
| page | 0 |  |



#### Response


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Authorization | {{token}} | (Required)  |

***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| size | 3 |  |
| page | 0 |  |


***Body:***

```json
{
    "total": 4,
    "page": 2,
    "items": [
        {
            "id": 1,
            "deletedDate": null,
            "createdDate": "2021-11-22T05:39:12.000+00:00",
            "name": "Test2"
        },
        {
            "id": 2,
            "deletedDate": null,
            "createdDate": "2021-11-22T04:13:26.000+00:00",
            "name": "Test3"
        },
        {
            "id": 3,
            "deletedDate": null,
            "createdDate": "2021-11-22T04:52:19.000+00:00",
            "name": "Test4"
        }
    ]
}
```

***Status Code:*** 200

<br>



### 4. GET permission/name/{name}



***Endpoint:***

```bash
Method: GET
Type: 
URL: {{urlauth2}}/permission/name/:name
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Authorization | {{token}} |  |



***URL variables:***

| Key | Value | Description |
| --- | ------|-------------|
| name | Test2 |  |

#### Response

```json
{
    "id": 1,
    "deletedDate": null,
    "createdDate": "2021-11-22T02:33:39.000+00:00",
    "name": "Test2"
}
```

***Status Code:*** 200

| Key | Value | Description |
| --- | ------|-------------|
| name | Test7 |  |

```json
{
  "status": "NOT_FOUND",
  "message": "There is no permission with name: Test7",
  "statusCode": 404
}
```

***Status Code:*** 404



### 5. GET permission/{id}



***Endpoint:***

```bash
Method: GET
Type: 
URL: {{urlauth2}}/permission/:id
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Authorization | {{token}} |  |



***URL variables:***

| Key | Value | Description |
| --- | ------|-------------|
| id | 1 | (Required)  |



#### Response


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Authorization | {{token}} | (Required)  |



***Query:***

| Key | Value | Description |
| --- | ------|-------------|
| id | 1 | (Required)  |

***Body:***

```json
{
    "id": 1,
    "deletedDate": null,
    "createdDate": "2021-11-22T05:39:12.000+00:00",
    "name": "Test2"
}
```



***Status Code:*** 200

***Query:***

| Key | Value | Description |
| --- | ------|-------------|
| id | 100 | (Required)  |

***Body:***

```json
{
  "status": "NOT_FOUND",
  "message": "PermissionDTO was not found with id 100",
  "statusCode": 404
}
```



***Status Code:*** 404

<br>



### 6. POST permission



***Endpoint:***

```bash
Method: POST
Type: RAW
URL: {{urlauth2}}/permission
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Authorization | {{token}} |  |



***Body:***

```json        
{
    "name": "Test4"
}
```



#### Response


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Authorization | {{token}} | (Required)  |



***Body:***

```json
{
    "id": 4,
    "deletedDate": null,
    "createdDate": "2021-11-22T05:39:19.260+00:00",
    "name": "Test0"
}
```

***Status Code:*** 201

<br>



### 7. POST permission/validate



***Endpoint:***

```bash
Method: POST
Type: RAW
URL: {{urlauth2}}/permission/validate
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Authorization | {{token}} |  |



***Body:***

```json        
{
    "permissions":[{
        "name": "Test4"
    }]
}
```



#### Response


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Authorization | {{token}} | (Required)  |



***Body:***

```json
{
    "message": "The user can perform the action"
}
```

***Status Code:*** 200

***Body:***

```json
{
  "status": "FORBIDDEN",
  "message": "The privileged does not count with the permission Test4 because it was removed from the privileged",
  "statusCode": 403
}
```

```json
{
  "status": "FORBIDDEN",
  "message": "The privileged does not count with the permission Test10",
  "statusCode": 403
}
```

***Status Code:*** 403

<br>



### 8. PUT permission



***Endpoint:***

```bash
Method: PUT
Type: RAW
URL: {{urlauth2}}/permission
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Authorization | {{token}} |  |



***Body:***

```json        
{
    "id": 1,
    "name": "Test2"
    "deletedDate": null
}
```



#### Response


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Authorization | {{token}} | (Required)  |



***Body:***

```json
{
    "id": 1,
    "deletedDate": null,
    "createdDate": "2021-11-22T05:39:12.408+00:00",
    "name": "Test2"
}
```

***Status Code:*** 200

<br>



## Privileged



### 1. DELETE privileged/{id}



***Endpoint:***

```bash
Method: DELETE
Type: 
URL: {{urlauth2}}/privileged/:id
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Authorization | {{token}} |  |



***URL variables:***

| Key | Value | Description |
| --- | ------|-------------|
| id | 2 | (Required)  |



#### Response


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Authorization | {{token}} | (Required)  |



***Query:***

| Key | Value | Description |
| --- | ------|-------------|
| id | <int64> | (Required)  |



***Status Code:*** 200

<br>



### 2. GET privileged



***Endpoint:***

```bash
Method: GET
Type: 
URL: {{urlauth2}}/privileged
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Authorization | {{token}} |  |



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| size | 2 |  |
| page | 0 |  |



#### Response


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Authorization | {{token}} | (Required)  |



***Body:***

```json
{
    "total": 2,
    "page": 1,
    "items": [
        {
            "id": 1,
            "deletedDate": null,
            "createdDate": "2021-11-22T02:40:09.000+00:00",
            "privilegedPermissions": [
                {
                    "id": 1,
                    "deletedDate": null,
                    "createdDate": "2021-11-22T03:45:09.000+00:00",
                    "permission": {
                        "id": 1,
                        "deletedDate": null,
                        "createdDate": "2021-11-22T02:33:39.000+00:00",
                        "name": "Test2"
                    },
                    "idResponsible": "619add3054d4e47e94e8bc0c"
                },
                {
                    "id": 5,
                    "deletedDate": "2021-11-22T04:58:47.000+00:00",
                    "createdDate": "2021-11-22T04:57:12.000+00:00",
                    "permission": {
                        "id": 3,
                        "deletedDate": null,
                        "createdDate": "2021-11-22T04:52:19.000+00:00",
                        "name": "Test4"
                    },
                    "idResponsible": "619add3054d4e47e94e8bc0c"
                }
            ],
            "idPrivileged": "619add3054d4e47e94e8bc0c"
        },
        {
            "id": 2,
            "deletedDate": null,
            "createdDate": "2021-11-22T04:00:24.000+00:00",
            "privilegedPermissions": [
                {
                    "id": 2,
                    "deletedDate": null,
                    "createdDate": "2021-11-22T04:00:33.000+00:00",
                    "permission": {
                        "id": 1,
                        "deletedDate": null,
                        "createdDate": "2021-11-22T02:33:39.000+00:00",
                        "name": "Test2"
                    },
                    "idResponsible": "619add3054d4e47e94e8bc0c"
                },
                {
                    "id": 3,
                    "deletedDate": null,
                    "createdDate": "2021-11-22T04:13:34.000+00:00",
                    "permission": {
                        "id": 2,
                        "deletedDate": null,
                        "createdDate": "2021-11-22T04:13:26.000+00:00",
                        "name": "Test3"
                    },
                    "idResponsible": "619add3054d4e47e94e8bc0c"
                },
                {
                    "id": 4,
                    "deletedDate": null,
                    "createdDate": "2021-11-22T04:56:29.000+00:00",
                    "permission": {
                        "id": 3,
                        "deletedDate": null,
                        "createdDate": "2021-11-22T04:52:19.000+00:00",
                        "name": "Test4"
                    },
                    "idResponsible": "619add3054d4e47e94e8bc0c"
                }
            ],
            "idPrivileged": "admin"
        }
    ]
}
```

***Status Code:*** 200

<br>



### 3. GET privileged/all



***Endpoint:***

```bash
Method: GET
Type: 
URL: {{urlauth2}}/privileged/all
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Authorization | {{token}} |  |



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| size | 2 |  |
| page | 0 |  |



#### Response


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Authorization | {{token}} | (Required)  |



***Body:***

```json
{
    "total": 2,
    "page": 1,
    "items": [
        {
            "id": 1,
            "deletedDate": null,
            "createdDate": "2021-11-22T02:40:09.000+00:00",
            "privilegedPermissions": [
                {
                    "id": 1,
                    "deletedDate": null,
                    "createdDate": "2021-11-22T03:45:09.000+00:00",
                    "permission": {
                        "id": 1,
                        "deletedDate": null,
                        "createdDate": "2021-11-22T05:39:12.000+00:00",
                        "name": "Test2"
                    },
                    "idResponsible": "619add3054d4e47e94e8bc0c"
                },
                {
                    "id": 5,
                    "deletedDate": "2021-11-22T04:58:47.000+00:00",
                    "createdDate": "2021-11-22T04:57:12.000+00:00",
                    "permission": {
                        "id": 3,
                        "deletedDate": null,
                        "createdDate": "2021-11-22T04:52:19.000+00:00",
                        "name": "Test4"
                    },
                    "idResponsible": "619add3054d4e47e94e8bc0c"
                }
            ],
            "idPrivileged": "619add3054d4e47e94e8bc0c"
        },
        {
            "id": 2,
            "deletedDate": null,
            "createdDate": "2021-11-22T05:06:46.000+00:00",
            "privilegedPermissions": [
                {
                    "id": 2,
                    "deletedDate": null,
                    "createdDate": "2021-11-22T04:00:33.000+00:00",
                    "permission": {
                        "id": 1,
                        "deletedDate": null,
                        "createdDate": "2021-11-22T05:39:12.000+00:00",
                        "name": "Test2"
                    },
                    "idResponsible": "619add3054d4e47e94e8bc0c"
                },
                {
                    "id": 3,
                    "deletedDate": null,
                    "createdDate": "2021-11-22T04:13:34.000+00:00",
                    "permission": {
                        "id": 2,
                        "deletedDate": null,
                        "createdDate": "2021-11-22T04:13:26.000+00:00",
                        "name": "Test3"
                    },
                    "idResponsible": "619add3054d4e47e94e8bc0c"
                },
                {
                    "id": 4,
                    "deletedDate": null,
                    "createdDate": "2021-11-22T04:56:29.000+00:00",
                    "permission": {
                        "id": 3,
                        "deletedDate": null,
                        "createdDate": "2021-11-22T04:52:19.000+00:00",
                        "name": "Test4"
                    },
                    "idResponsible": "619add3054d4e47e94e8bc0c"
                }
            ],
            "idPrivileged": "admin"
        }
    ]
}
```

***Status Code:*** 200

<br>



### 4. GET privileged/external_id/{id}



***Endpoint:***

```bash
Method: GET
Type: 
URL: {{urlauth2}}/privileged/external_id/:id
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Authorization | {{token}} |  |



***URL variables:***

| Key | Value | Description |
| --- | ------|-------------|
| id | admin |  |

#### Response


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Authorization | {{token}} | (Required)  |

***URL variables:***

| Key | Value | Description |
| --- | ------|-------------|
| id | admin |  |



***Body:***

```json
{
  "id": 2,
  "deletedDate": null,
  "createdDate": "2021-11-22T04:00:24.000+00:00",
  "privilegedPermissions": [
    {
      "id": 2,
      "deletedDate": null,
      "createdDate": "2021-11-22T04:00:33.000+00:00",
      "permission": {
        "id": 1,
        "deletedDate": null,
        "createdDate": "2021-11-22T02:33:39.000+00:00",
        "name": "Test2"
      },
      "idResponsible": "619add3054d4e47e94e8bc0c"
    },
    {
      "id": 3,
      "deletedDate": null,
      "createdDate": "2021-11-22T04:13:34.000+00:00",
      "permission": {
        "id": 2,
        "deletedDate": null,
        "createdDate": "2021-11-22T04:13:26.000+00:00",
        "name": "Test3"
      },
      "idResponsible": "619add3054d4e47e94e8bc0c"
    },
    {
      "id": 4,
      "deletedDate": null,
      "createdDate": "2021-11-22T04:56:29.000+00:00",
      "permission": {
        "id": 3,
        "deletedDate": null,
        "createdDate": "2021-11-22T04:52:19.000+00:00",
        "name": "Test4"
      },
      "idResponsible": "619add3054d4e47e94e8bc0c"
    }
  ],
  "idPrivileged": "admin"
}
```

***Status Code:*** 200

***URL variables:***

| Key | Value | Description |
| --- | ------|-------------|
| id | 2 |  |



***Body:***

```json
{
  "status": "NOT_FOUND",
  "message": "There is no privilege with id: 2",
  "statusCode": 404
}
```

***Status Code:*** 404

### 5. GET privileged/{id}



***Endpoint:***

```bash
Method: GET
Type: 
URL: {{urlauth2}}/privileged/:id
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Authorization | {{token}} |  |



***URL variables:***

| Key | Value | Description |
| --- | ------|-------------|
| id | 2 | (Required)  |



#### Response


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Authorization | {{token}} | (Required)  |



***Query:***

| Key | Value | Description |
| --- | ------|-------------|
| id | 2 | (Required)  |

```json
{
    "id": 2,
    "deletedDate": "2021-11-22T05:05:19.000+00:00",
    "createdDate": "2021-11-22T04:00:24.000+00:00",
    "privilegedPermissions": [
        {
            "id": 2,
            "deletedDate": null,
            "createdDate": "2021-11-22T04:00:33.000+00:00",
            "permission": {
                "id": 1,
                "deletedDate": null,
                "createdDate": "2021-11-22T02:33:39.000+00:00",
                "name": "Test2"
            },
            "idResponsible": "619add3054d4e47e94e8bc0c"
        },
        {
            "id": 3,
            "deletedDate": null,
            "createdDate": "2021-11-22T04:13:34.000+00:00",
            "permission": {
                "id": 2,
                "deletedDate": null,
                "createdDate": "2021-11-22T04:13:26.000+00:00",
                "name": "Test3"
            },
            "idResponsible": "619add3054d4e47e94e8bc0c"
        },
        {
            "id": 4,
            "deletedDate": null,
            "createdDate": "2021-11-22T04:56:29.000+00:00",
            "permission": {
                "id": 3,
                "deletedDate": null,
                "createdDate": "2021-11-22T04:52:19.000+00:00",
                "name": "Test4"
            },
            "idResponsible": "619add3054d4e47e94e8bc0c"
        }
    ],
    "idPrivileged": "admin"
}
```



***Status Code:*** 200

***Query:***

| Key | Value | Description |
| --- | ------|-------------|
| id | 3 | (Required)  |

```json
{
  "status": "NOT_FOUND",
  "message": "PrivilegedDTO was not found with id 3",
  "statusCode": 404
}
```



***Status Code:*** 404

<br>



### 6. POST privileged



***Endpoint:***

```bash
Method: POST
Type: RAW
URL: {{urlauth2}}/privileged
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Authorization | {{token}} |  |



***Body:***

```json        
{
    "idPrivileged": "admin"
}
```



#### Response


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Authorization | {{token}} | (Required)  |



***Body:***

```json
{
    "id": 2,
    "deletedDate": null,
    "createdDate": "2021-11-22T04:00:24.180+00:00",
    "privilegedPermissions": null,
    "idPrivileged": "admin"
}
```

***Status Code:*** 201

<br>



### 7. PUT privileged



***Endpoint:***

```bash
Method: PUT
Type: RAW
URL: {{urlauth2}}/privileged
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Authorization | {{token}} |  |



***Body:***

```json        
{
    "id": 2,
    "deletedDate": null,
    "privilegedPermissions": [
        {
            "id": 2,
            "deletedDate": null,
            "createdDate": "2021-11-22T04:00:33.000+00:00",
            "permission": {
                "id": 1,
                "deletedDate": null,
                "createdDate": "2021-11-22T02:33:39.000+00:00",
                "name": "Test2"
            },
            "idResponsible": "619add3054d4e47e94e8bc0c"
        },
        {
            "id": 3,
            "deletedDate": null,
            "createdDate": "2021-11-22T04:13:34.000+00:00",
            "permission": {
                "id": 2,
                "deletedDate": null,
                "createdDate": "2021-11-22T04:13:26.000+00:00",
                "name": "Test3"
            },
            "idResponsible": "619add3054d4e47e94e8bc0c"
        },
        {
            "id": 4,
            "deletedDate": null,
            "createdDate": "2021-11-22T04:56:29.000+00:00",
            "permission": {
                "id": 3,
                "deletedDate": null,
                "createdDate": "2021-11-22T04:52:19.000+00:00",
                "name": "Test4"
            },
            "idResponsible": "619add3054d4e47e94e8bc0c"
        }
    ],
    "idPrivileged": "admin"
}
```



#### Response


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Authorization | {{token}} | (Required)  |



***Body:***

```json
{
    "id": 2,
    "deletedDate": null,
    "createdDate": "2021-11-22T05:06:45.865+00:00",
    "privilegedPermissions": [
        {
            "id": 2,
            "deletedDate": null,
            "createdDate": "2021-11-22T04:00:33.000+00:00",
            "permission": {
                "id": 1,
                "deletedDate": null,
                "createdDate": "2021-11-22T02:33:39.000+00:00",
                "name": "Test2"
            },
            "idResponsible": "619add3054d4e47e94e8bc0c"
        },
        {
            "id": 3,
            "deletedDate": null,
            "createdDate": "2021-11-22T04:13:34.000+00:00",
            "permission": {
                "id": 2,
                "deletedDate": null,
                "createdDate": "2021-11-22T04:13:26.000+00:00",
                "name": "Test3"
            },
            "idResponsible": "619add3054d4e47e94e8bc0c"
        },
        {
            "id": 4,
            "deletedDate": null,
            "createdDate": "2021-11-22T04:56:29.000+00:00",
            "permission": {
                "id": 3,
                "deletedDate": null,
                "createdDate": "2021-11-22T04:52:19.000+00:00",
                "name": "Test4"
            },
            "idResponsible": "619add3054d4e47e94e8bc0c"
        }
    ],
    "idPrivileged": "admin"
}
```

***Status Code:*** 200

<br>



### 8. PUT privileged/assign_permission



***Endpoint:***

```bash
Method: PUT
Type: RAW
URL: {{urlauth2}}/privileged/assign_permission
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Authorization | {{token}} |  |



***Body:***

```json        
{
    "id": "admin",
    "permissions":[{
        "name": "Test4"
    }]
}
```



#### Response


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Authorization | {{token}} | (Required)  |



***Body:***

```json
{
    "id": 2,
    "deletedDate": null,
    "createdDate": "2021-11-22T04:00:24.000+00:00",
    "privilegedPermissions": [
        {
            "id": 2,
            "deletedDate": null,
            "createdDate": "2021-11-22T04:00:33.000+00:00",
            "permission": {
                "id": 1,
                "deletedDate": null,
                "createdDate": "2021-11-22T02:33:39.000+00:00",
                "name": "Test2"
            },
            "idResponsible": "619add3054d4e47e94e8bc0c"
        },
        {
            "id": 3,
            "deletedDate": null,
            "createdDate": "2021-11-22T04:13:34.000+00:00",
            "permission": {
                "id": 2,
                "deletedDate": null,
                "createdDate": "2021-11-22T04:13:26.000+00:00",
                "name": "Test3"
            },
            "idResponsible": "619add3054d4e47e94e8bc0c"
        },
        {
            "id": 4,
            "deletedDate": null,
            "createdDate": "2021-11-22T04:56:29.131+00:00",
            "permission": {
                "id": 3,
                "deletedDate": null,
                "createdDate": "2021-11-22T04:52:19.000+00:00",
                "name": "Test4"
            },
            "idResponsible": "619add3054d4e47e94e8bc0c"
        }
    ],
    "idPrivileged": "admin"
}
```

***Status Code:*** 200

<br>



### 9. PUT privileged/unassign_permission



***Endpoint:***

```bash
Method: PUT
Type: RAW
URL: {{urlauth2}}/privileged/unassign_permission
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Authorization | {{token}} |  |



***Body:***

```json        
{
    "id": "619add3054d4e47e94e8bc0c",
    "permissions":[{
        "name": "Test5"
    }]
}
```



#### Response


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Authorization | {{token}} | (Required)  |



***Body:***

```json
{
    "id": 1,
    "deletedDate": null,
    "createdDate": "2021-11-22T02:40:09.000+00:00",
    "privilegedPermissions": [
        {
            "id": 1,
            "deletedDate": null,
            "createdDate": "2021-11-22T03:45:09.000+00:00",
            "permission": {
                "id": 1,
                "deletedDate": null,
                "createdDate": "2021-11-22T02:33:39.000+00:00",
                "name": "Test2"
            },
            "idResponsible": "619add3054d4e47e94e8bc0c"
        },
        {
            "id": 5,
            "deletedDate": "2021-11-22T04:58:47.367+00:00",
            "createdDate": "2021-11-22T04:57:12.000+00:00",
            "permission": {
                "id": 3,
                "deletedDate": null,
                "createdDate": "2021-11-22T04:52:19.000+00:00",
                "name": "Test4"
            },
            "idResponsible": "619add3054d4e47e94e8bc0c"
        }
    ],
    "idPrivileged": "619add3054d4e47e94e8bc0c"
}
```

***Status Code:*** 200

<br>



***Available Variables:***

| Key | Value | Type |
| --- | ------|-------------|
| baseUrl | http://localhost:8080/auth2/v1 |  |



---
[Back to top](#auth2-api)
> Made with &#9829; by [thedevsaddam](https://github.com/thedevsaddam) | Generated at: 2021-11-22 02:26:25 by [docgen](https://github.com/thedevsaddam/docgen)
