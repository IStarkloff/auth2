package com.starkloff.auth2.services.impl;


import com.starkloff.auth2.common.service.impl.ModelEntityServiceImpl;
import com.starkloff.auth2.dto.PermissionDTO;
import com.starkloff.auth2.dto.PrivilegedDTO;
import com.starkloff.auth2.dto.PrivilegedPermissionDTO;
import com.starkloff.auth2.exceptions.ForbiddenException;
import com.starkloff.auth2.mappers.PrivilegedPermissionMapper;
import com.starkloff.auth2.models.Permission;
import com.starkloff.auth2.models.PrivilegedPermission;
import com.starkloff.auth2.payload.request.ValidatePermissionRequest;
import com.starkloff.auth2.repositories.PrivilegedPermissionRepository;
import com.starkloff.auth2.services.PermissionService;
import com.starkloff.auth2.services.PrivilegedPermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class PrivilegedPermissionServiceImpl extends ModelEntityServiceImpl<PrivilegedPermission, PrivilegedPermissionDTO, PrivilegedPermissionRepository, PrivilegedPermissionMapper> implements PrivilegedPermissionService {

    private final PermissionService permissionService;

    public PrivilegedPermissionServiceImpl(PrivilegedPermissionRepository repository, PrivilegedPermissionMapper mapper, PermissionService permissionService) {
        super(PrivilegedPermission.class, PrivilegedPermissionDTO.class, repository, mapper);
        this.permissionService = permissionService;
    }

    @Override
    public PrivilegedDTO assignPermissions(PrivilegedDTO privilegedDTO, List<PermissionDTO> permissions, String idResponsible) throws Exception {
        for (PermissionDTO permissionDTO : permissions) {
            PermissionDTO permission = this.permissionService.getPermissionByName(permissionDTO.getName());
            Optional<PrivilegedPermissionDTO> result = privilegedDTO.getPrivilegedPermissions().stream().filter(privilegedPermissionDTO -> privilegedPermissionDTO.getPermission().getName().equals(permissionDTO.getName())).findFirst();
            if (result.isPresent()) {
                result.get().setDeletedDate(null);
                result.get().setIdResponsible(idResponsible);
            } else {
                PrivilegedPermissionDTO privilegedPermissionDTO = new PrivilegedPermissionDTO(permission, idResponsible);
                privilegedDTO.getPrivilegedPermissions().add(privilegedPermissionDTO);
            }
        }
        return privilegedDTO;
    }

    @Override
    public PrivilegedDTO unassignPermissions(PrivilegedDTO privilegedDTO, List<PermissionDTO> permissions, String idResponsible) throws Exception {
        for (PermissionDTO permissionDTO : permissions) {
            PermissionDTO permission = this.permissionService.getPermissionByName(permissionDTO.getName());
            Optional<PrivilegedPermissionDTO> result = privilegedDTO.getPrivilegedPermissions().stream().filter(privilegedPermissionDTO -> privilegedPermissionDTO.getPermission().getName().equals(permissionDTO.getName())).findFirst();
            if (result.isPresent()) {
                result.get().setDeletedDate(new Date());
                result.get().setIdResponsible(idResponsible);
            } else {
                PrivilegedPermissionDTO privilegedPermissionDTO = new PrivilegedPermissionDTO(permission, idResponsible);
                privilegedPermissionDTO.setDeletedDate(new Date());
                privilegedDTO.getPrivilegedPermissions().add(privilegedPermissionDTO);
            }
        }
        return privilegedDTO;
    }

    @Override
    public void validatePermission(PrivilegedDTO privilegedDTO, ValidatePermissionRequest validatePermissionRequest) throws Exception {
        for (PermissionDTO permissionDTO : validatePermissionRequest.getPermissions()) {
            PermissionDTO permission = this.permissionService.getPermissionByName(permissionDTO.getName());
            Optional<PrivilegedPermission> privilegedPermission = this.repository.findByPrivilegedIdAndPermissionId(privilegedDTO.getId(), permission.getId());
            if (privilegedPermission.isEmpty()) {
                throw new ForbiddenException("The privileged does not count with the permission " + permissionDTO.getName());
            } else {
                if (privilegedPermission.get().getDeletedDate() != null) {
                    throw new ForbiddenException("The privileged does not count with the permission " + permissionDTO.getName() + " because it was removed from the privileged");
                }
            }
        }
    }

    @Override
    public void validateDeletedPermission(PrivilegedDTO privilegedDTO, ValidatePermissionRequest validatePermissionRequest) throws Exception {
        for (PermissionDTO permissionDTO : validatePermissionRequest.getPermissions()) {
            PermissionDTO permission = this.permissionService.getPermissionByName(permissionDTO.getName());
            Optional<PrivilegedPermission> privilegedPermission = this.repository.findByPrivilegedIdAndPermissionId(privilegedDTO.getId(), permission.getId());
            if (privilegedPermission.isPresent()) {
                if (privilegedPermission.get().getDeletedDate() != null) {
                    throw new ForbiddenException("The privileged does not count with the permission " + permissionDTO.getName() + " because it was removed from the privileged");
                }
            }
        }
    }

}
