package com.starkloff.auth2.services.impl;

import com.starkloff.auth2.common.mapper.MappingContext;
import com.starkloff.auth2.common.service.impl.ModelEntityServiceImpl;
import com.starkloff.auth2.dto.PrivilegedDTO;
import com.starkloff.auth2.exceptions.NotAcceptableException;
import com.starkloff.auth2.mappers.PrivilegedMapper;
import com.starkloff.auth2.models.Privileged;
import com.starkloff.auth2.repositories.PrivilegedRepository;
import com.starkloff.auth2.services.PrivilegedService;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class PrivilegedServiceImpl extends ModelEntityServiceImpl<Privileged, PrivilegedDTO, PrivilegedRepository, PrivilegedMapper> implements PrivilegedService {
    public PrivilegedServiceImpl(PrivilegedRepository repository, PrivilegedMapper mapper) {
        super(Privileged.class, PrivilegedDTO.class, repository, mapper);
    }

    @Override
    public PrivilegedDTO getPrivilegedByIdPrivileged(String id) throws Exception {
        Optional<Privileged> optionalPrivileged = this.repository.findByIdPrivileged(id);
        if (optionalPrivileged.isPresent()) {
            if (optionalPrivileged.get().getDeletedDate() != null) {
                throw new NotAcceptableException("The privileged is inactive, please activate it again");
            }
            return this.mapper.toDto(optionalPrivileged.get(), new MappingContext());
        }
        throw new NoSuchElementException("There is no privilege with id: " + id);
    }
}
