package com.starkloff.auth2.services.impl;

import com.starkloff.auth2.common.mapper.MappingContext;
import com.starkloff.auth2.common.service.impl.ModelEntityServiceImpl;
import com.starkloff.auth2.dto.PermissionDTO;
import com.starkloff.auth2.exceptions.NotAcceptableException;
import com.starkloff.auth2.mappers.PermissionMapper;
import com.starkloff.auth2.models.Permission;
import com.starkloff.auth2.repositories.PermissionRepository;
import com.starkloff.auth2.services.PermissionService;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class PermissionServiceImpl extends ModelEntityServiceImpl<Permission, PermissionDTO, PermissionRepository, PermissionMapper> implements PermissionService {

    public PermissionServiceImpl(PermissionRepository repository, PermissionMapper mapper) {
        super(Permission.class, PermissionDTO.class, repository, mapper);
    }


    @Override
    public PermissionDTO getPermissionByName(String name) {
        Optional<Permission> optionalPermission = this.repository.findByName(name);
        if (optionalPermission.isEmpty()) {
            throw new NoSuchElementException("There is no permission with name: " + name);
        }
        if (optionalPermission.get().getDeletedDate() != null) {
            throw new NotAcceptableException("The permission is inactive, please activate it again");
        }
        return this.mapper.toDto(optionalPermission.get(), new MappingContext());

    }
}
