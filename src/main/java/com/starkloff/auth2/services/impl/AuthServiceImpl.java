package com.starkloff.auth2.services.impl;

import com.starkloff.auth2.exceptions.ForbiddenException;
import com.starkloff.auth2.payload.response.UserResponse;
import com.starkloff.auth2.services.AuthService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class AuthServiceImpl implements AuthService {

    @Autowired
    private RestTemplate restTemplate;

    private HttpHeaders headers = new HttpHeaders();

    private final String uriAuth = System.getenv().get("URL_AUTH");

    @Override
    public UserResponse validateAction(String authorization) throws Exception {
        final String uri = uriAuth + "/users/current";
        this.headers.set("Authorization", "bearer " + authorization);
        final HttpEntity<String> entity = new HttpEntity<String>(headers);
        ResponseEntity<UserResponse> result = restTemplate.exchange(uri, HttpMethod.GET, entity, UserResponse.class);
        return result.getBody();
    }

    @Override
    public UserResponse validateActionAdmin(String authorization) throws Exception {
        UserResponse userResponse = this.validateAction(authorization);
        if (userResponse.getPermissions().stream().noneMatch(permission -> permission.trim().equals("admin"))) {
            throw new ForbiddenException("You should have administrator permission to perform this action");
        }
        return userResponse;
    }
}
