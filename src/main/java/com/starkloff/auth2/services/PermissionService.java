package com.starkloff.auth2.services;

import com.starkloff.auth2.common.service.ModelEntityService;
import com.starkloff.auth2.dto.PermissionDTO;

public interface PermissionService extends ModelEntityService<PermissionDTO> {
    PermissionDTO getPermissionByName(String name);
}
