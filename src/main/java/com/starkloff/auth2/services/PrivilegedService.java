package com.starkloff.auth2.services;

import com.starkloff.auth2.common.service.ModelEntityService;
import com.starkloff.auth2.dto.PrivilegedDTO;

public interface PrivilegedService extends ModelEntityService<PrivilegedDTO> {

    PrivilegedDTO getPrivilegedByIdPrivileged(String id) throws Exception;
}
