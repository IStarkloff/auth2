package com.starkloff.auth2.services;

import com.starkloff.auth2.common.service.ModelEntityService;
import com.starkloff.auth2.dto.PermissionDTO;
import com.starkloff.auth2.dto.PrivilegedDTO;
import com.starkloff.auth2.dto.PrivilegedPermissionDTO;
import com.starkloff.auth2.payload.request.ValidatePermissionRequest;

import java.util.List;

public interface PrivilegedPermissionService extends ModelEntityService<PrivilegedPermissionDTO> {

    PrivilegedDTO assignPermissions(PrivilegedDTO privilegedDTO, List<PermissionDTO> permissions, String idResponsible) throws Exception;

    PrivilegedDTO unassignPermissions(PrivilegedDTO privilegedDTO, List<PermissionDTO> permissions, String idResponsible) throws Exception;

    void validatePermission(PrivilegedDTO privilegedDTO, ValidatePermissionRequest validatePermissionRequest) throws Exception;

    void validateDeletedPermission(PrivilegedDTO privilegedDTO, ValidatePermissionRequest validatePermissionRequest) throws Exception;

}
