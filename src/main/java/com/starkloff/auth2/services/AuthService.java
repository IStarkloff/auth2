package com.starkloff.auth2.services;

import com.starkloff.auth2.payload.response.UserResponse;

public interface AuthService {
    UserResponse validateAction(String authorization) throws Exception;

    UserResponse validateActionAdmin(String authorization) throws Exception;
}
