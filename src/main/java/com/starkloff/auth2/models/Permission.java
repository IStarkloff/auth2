package com.starkloff.auth2.models;

import com.starkloff.auth2.common.model.ModelEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import static com.starkloff.auth2.constants.SQLConstanst.PERMISSION;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = PERMISSION)
public class Permission extends ModelEntity {
    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @Override
    public Class<? extends ModelEntity> realType() {
        return Permission.class;
    }
}
