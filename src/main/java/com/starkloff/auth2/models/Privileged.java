package com.starkloff.auth2.models;

import static com.starkloff.auth2.constants.SQLConstanst.PRIVILEGED;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.starkloff.auth2.common.model.ModelEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = PRIVILEGED)
public class Privileged extends ModelEntity {

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "id_privileged", nullable = false)
    private List<PrivilegedPermission> privilegedPermissions;

    @Column(name = "id_privileged", unique = true, nullable = false)
    private String idPrivileged;

    @Override
    public Class<? extends ModelEntity> realType() {
        return Privileged.class;
    }
}
