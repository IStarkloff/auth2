package com.starkloff.auth2.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.starkloff.auth2.common.model.ModelEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

import static com.starkloff.auth2.constants.SQLConstanst.PRIVILEGIADO_PERMISO;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = PRIVILEGIADO_PERMISO)
public class PrivilegedPermission extends ModelEntity {

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_permission", nullable = false)
    Permission permission;

    @Column(name = "id_responsable")
    private String idResponsible;

    @Override
    public Class<? extends ModelEntity> realType() {
        return PrivilegedPermission.class;
    }
}
