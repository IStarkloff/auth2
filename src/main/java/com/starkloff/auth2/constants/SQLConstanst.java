package com.starkloff.auth2.constants;

public abstract class SQLConstanst {
    public static final String PERMISSION = "permissions";
    public static final String PRIVILEGED = "privileged";
    public static final String PRIVILEGIADO_PERMISO = "privileged_permission";
}
