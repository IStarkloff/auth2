package com.starkloff.auth2.mappers;

import com.starkloff.auth2.common.mapper.EntityMapper;
import com.starkloff.auth2.common.mapper.MappingContext;
import com.starkloff.auth2.common.repository.ModelEntityRepository;
import com.starkloff.auth2.dto.PrivilegedPermissionDTO;
import com.starkloff.auth2.models.PrivilegedPermission;
import org.springframework.stereotype.Component;

@Component
public class PrivilegedPermissionMapper extends EntityMapper<PrivilegedPermission, PrivilegedPermissionDTO> {
    PermissionMapper permissionMapper;

    public PrivilegedPermissionMapper(ModelEntityRepository<PrivilegedPermission> repository, PermissionMapper permissionMapper) {
        super(repository, PrivilegedPermission.class, PrivilegedPermissionDTO.class);
        this.permissionMapper = permissionMapper;
    }

    @Override
    public void mapToDto(PrivilegedPermission source, PrivilegedPermissionDTO destination, MappingContext context) {
        destination.setId(source.getId());
        destination.setDeletedDate(source.getDeletedDate());
        destination.setCreatedDate(source.getCreatedDate());
        destination.setIdResponsible(source.getIdResponsible());
        destination.setPermission(this.permissionMapper.toDto(source.getPermission(), new MappingContext()));
    }

    @Override
    public void mapToModel(PrivilegedPermissionDTO source, PrivilegedPermission destination, MappingContext context) {
        destination.setId(source.getId());
        destination.setDeletedDate(source.getDeletedDate());
        destination.setCreatedDate(source.getCreatedDate());
        destination.setIdResponsible(source.getIdResponsible());
        destination.setPermission(this.permissionMapper.toModel(source.getPermission(), new MappingContext()));
    }
}
