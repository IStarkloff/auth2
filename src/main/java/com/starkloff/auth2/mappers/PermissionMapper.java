package com.starkloff.auth2.mappers;

import com.starkloff.auth2.common.mapper.EntityMapper;
import com.starkloff.auth2.common.mapper.MappingContext;
import com.starkloff.auth2.dto.PermissionDTO;
import com.starkloff.auth2.models.Permission;
import com.starkloff.auth2.repositories.PermissionRepository;
import org.springframework.stereotype.Component;

@Component
public class PermissionMapper extends EntityMapper<Permission, PermissionDTO> {
    public PermissionMapper(PermissionRepository repository) {
        super(repository, Permission.class, PermissionDTO.class);
    }

    @Override
    public void mapToDto(Permission source, PermissionDTO destination, MappingContext context) {
        destination.setId(source.getId());
        destination.setName(source.getName());
        destination.setDeletedDate(source.getDeletedDate());
        destination.setCreatedDate(source.getCreatedDate());
    }

    @Override
    public void mapToModel(PermissionDTO source, Permission destination, MappingContext context) {
        destination.setId(source.getId());
        destination.setName(source.getName());
        destination.setDeletedDate(source.getDeletedDate());
        destination.setCreatedDate(source.getCreatedDate());
        destination.setPrimaryKey(source.getName());
        destination.setNamePrimaryKey("name");
    }
}
