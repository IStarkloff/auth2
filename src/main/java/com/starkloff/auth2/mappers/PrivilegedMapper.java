package com.starkloff.auth2.mappers;

import com.starkloff.auth2.common.mapper.EntityMapper;
import com.starkloff.auth2.common.mapper.MappingContext;
import com.starkloff.auth2.dto.PrivilegedDTO;
import com.starkloff.auth2.models.Privileged;
import com.starkloff.auth2.repositories.PrivilegedRepository;
import org.springframework.stereotype.Component;

@Component
public class PrivilegedMapper extends EntityMapper<Privileged, PrivilegedDTO> {

    PrivilegedPermissionMapper privilegedPermissionMapper;

    public PrivilegedMapper(PrivilegedRepository repository, PrivilegedPermissionMapper privilegedPermissionMapper) {
        super(repository, Privileged.class, PrivilegedDTO.class);
        this.privilegedPermissionMapper = privilegedPermissionMapper;
    }

    @Override
    public void mapToDto(Privileged source, PrivilegedDTO destination, MappingContext context) {
        destination.setId(source.getId());
        destination.setDeletedDate(source.getDeletedDate());
        destination.setCreatedDate(source.getCreatedDate());
        destination.setIdPrivileged(source.getIdPrivileged());
        destination.setPrivilegedPermissions(this.privilegedPermissionMapper.mapCollectionToDtos(source.getPrivilegedPermissions(), new MappingContext()));
    }

    @Override
    public void mapToModel(PrivilegedDTO source, Privileged destination, MappingContext context) {
        destination.setId(source.getId());
        destination.setDeletedDate(source.getDeletedDate());
        destination.setCreatedDate(source.getCreatedDate());
        destination.setIdPrivileged(source.getIdPrivileged());
        destination.setPrimaryKey(source.getIdPrivileged());
        destination.setNamePrimaryKey("identifier");
        destination.setPrivilegedPermissions(this.privilegedPermissionMapper.mapCollectionToModels(source.getPrivilegedPermissions(), new MappingContext()));
    }
}
