package com.starkloff.auth2.payload.request;

import com.starkloff.auth2.dto.PermissionDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AssignPermissionRequest {
    @NotEmpty(message = "The privileged id is required")
    private String id;
    @NotEmpty(message = "At least one permission is required")
    private List<PermissionDTO> permissions;
}
