package com.starkloff.auth2.errors;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

import com.starkloff.auth2.exceptions.MyRestTemplateException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;

public class MyApiRestTemplateErrorHandler extends DefaultResponseErrorHandler {

    @Override
    public void handleError(ClientHttpResponse response) throws IOException {

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(response.getBody()))) {
            String httpBodyResponse = reader.lines().collect(Collectors.joining(""));
            if (response.getStatusCode().is4xxClientError()) {
                ApiError apiError = new ApiError(response.getStatusCode(), response.getStatusText(), response.getStatusCode().value());
                throw new MyRestTemplateException(httpBodyResponse, apiError);
            } else {
                JSONObject jsonError = new JSONObject(httpBodyResponse);
                String message = jsonError.get("message").toString();
                ApiError apiError = new ApiError(response.getStatusCode(), message, response.getStatusCode().value());
                throw new MyRestTemplateException(httpBodyResponse, apiError);
            }
        }
    }
}
