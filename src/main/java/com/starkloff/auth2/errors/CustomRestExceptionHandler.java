package com.starkloff.auth2.errors;

import java.util.NoSuchElementException;

import javax.naming.NoPermissionException;

import com.starkloff.auth2.exceptions.*;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@ControllerAdvice
public class CustomRestExceptionHandler extends ResponseEntityExceptionHandler {

    private ResponseEntity<Object> buildResponseEntity(ApiError apiError) {
        return new ResponseEntity<>(apiError, apiError.getStatus());
    }

    @ExceptionHandler(NoSuchElementException.class)
    protected ResponseEntity<Object> NotFound(NoSuchElementException ex) {
        ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, ex.getMessage(), HttpStatus.NOT_FOUND.value());
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(NoPermissionException.class)
    protected ResponseEntity<Object> NotPermission(NoPermissionException ex) {
        ApiError apiError = new ApiError(HttpStatus.UNAUTHORIZED, ex.getMessage(), HttpStatus.UNAUTHORIZED.value());
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(AlreadyExistException.class)
    protected ResponseEntity<Object> AlreadyExist(AlreadyExistException ex) {
        ApiError apiError = new ApiError(HttpStatus.CONFLICT, ex.getMessage(), HttpStatus.CONFLICT.value());
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(NotAcceptableException.class)
    protected ResponseEntity<Object> NotAcceptable(NotAcceptableException ex) {
        ApiError apiError = new ApiError(HttpStatus.NOT_ACCEPTABLE, ex.getMessage(), HttpStatus.NOT_ACCEPTABLE.value());
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(Exception.class)
    protected ResponseEntity<Object> Exception(Exception ex) {
        ApiError apiError = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value());
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(PreconditionsFailedExeption.class)
    protected ResponseEntity<Object> PreconditionsFailed(PreconditionsFailedExeption ex) {
        ApiError apiError = new ApiError(HttpStatus.PRECONDITION_FAILED, ex.getMessage(), HttpStatus.PRECONDITION_FAILED.value());
        return buildResponseEntity(apiError);
    }


    @ExceptionHandler(ForbiddenException.class)
    protected ResponseEntity<Object> BadCredentials(ForbiddenException ex) {
        ApiError apiError = new ApiError(HttpStatus.FORBIDDEN, ex.getMessage(), HttpStatus.FORBIDDEN.value());
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(BadRequestException.class)
    protected ResponseEntity<Object> BadRequest(BadRequestException ex) {
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, ex.getMessage(), HttpStatus.BAD_REQUEST.value());
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(MyRestTemplateException.class)
    protected ResponseEntity<Object> MyRestTemplate(MyRestTemplateException ex) {
        return buildResponseEntity(ex.getApiError());
    }

}
