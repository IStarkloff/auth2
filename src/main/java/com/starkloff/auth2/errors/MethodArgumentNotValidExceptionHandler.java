package com.starkloff.auth2.errors;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;

import lombok.Getter;
import lombok.Setter;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class MethodArgumentNotValidExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(code = HttpStatus.METHOD_NOT_ALLOWED)
    @ResponseBody
    public Error handleMethodArgumentNotValidException(MethodArgumentNotValidException ex, WebRequest request) {
        BindingResult result = ex.getBindingResult();

        List<String> errorList = new ArrayList<>();
        result.getFieldErrors().forEach((fieldError) -> {
            errorList.add(fieldError.getDefaultMessage());
        });
        result.getGlobalErrors().forEach((fieldError) -> {
            errorList.add(fieldError.getDefaultMessage());
        });

        return new Error(HttpStatus.METHOD_NOT_ALLOWED, errorList);
    }

    @Getter
    @Setter
    public static class Error {
        //    	private int errorCode;
//    	private String error;
//    	private String errorMessage;
        private List<String> message;

        public Error(HttpStatus status, List<String> fieldErrors) {
//    		this.errorCode = status.value();
//    		this.error = status.name();
//    		this.errorMessage = message;
            this.message = fieldErrors;
        }
    }
}
