package com.starkloff.auth2.controllers;

import com.starkloff.auth2.common.controller.ModelEntityController;
import com.starkloff.auth2.dto.PrivilegedDTO;
import com.starkloff.auth2.payload.request.AssignPermissionRequest;
import com.starkloff.auth2.payload.response.UserResponse;
import com.starkloff.auth2.services.AuthService;
import com.starkloff.auth2.services.PrivilegedPermissionService;
import com.starkloff.auth2.services.PrivilegedService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/privileged")
public class PrivilegedController extends ModelEntityController<PrivilegedDTO, PrivilegedService> {

    private final AuthService authService;
    private final PrivilegedPermissionService privilegedPermissionService;

    public PrivilegedController(PrivilegedService entityService, AuthService authService, PrivilegedPermissionService privilegedPermissionService) {
        super(entityService);
        this.authService = authService;
        this.privilegedPermissionService = privilegedPermissionService;
    }

    @PutMapping("/assign_permission")
    public ResponseEntity<PrivilegedDTO> assingPermission(@RequestHeader("Authorization") String authorization, @Valid @RequestBody AssignPermissionRequest assignPermission) throws Exception {
        UserResponse userResponse = this.authService.validateActionAdmin(authorization);
        PrivilegedDTO privilegedDTO = this.entityService.getPrivilegedByIdPrivileged(assignPermission.getId());
        privilegedDTO = this.privilegedPermissionService.assignPermissions(privilegedDTO, assignPermission.getPermissions(), userResponse.getId());
        return ResponseEntity.ok(this.entityService.update(privilegedDTO));
    }

    @PutMapping("/unassign_permission")
    public ResponseEntity<PrivilegedDTO> unassignPermission(@RequestHeader("Authorization") String authorization, @Valid @RequestBody AssignPermissionRequest assignPermission) throws Exception {
        UserResponse userResponse = this.authService.validateActionAdmin(authorization);
        PrivilegedDTO privilegedDTO = this.entityService.getPrivilegedByIdPrivileged(assignPermission.getId());
        privilegedDTO = this.privilegedPermissionService.unassignPermissions(privilegedDTO, assignPermission.getPermissions(), userResponse.getId());
        return ResponseEntity.ok(this.entityService.update(privilegedDTO));
    }

    @GetMapping("/external_id/{id}")
    public ResponseEntity<PrivilegedDTO> getByExternalId(@RequestHeader("Authorization") String authorization, @PathVariable String id) throws Exception {
        this.authService.validateAction(authorization);
        return ResponseEntity.ok(this.entityService.getPrivilegedByIdPrivileged(id));
    }


}
