package com.starkloff.auth2.controllers;

import com.starkloff.auth2.common.controller.ModelEntityController;
import com.starkloff.auth2.dto.PermissionDTO;
import com.starkloff.auth2.dto.PrivilegedDTO;
import com.starkloff.auth2.payload.request.ValidatePermissionRequest;
import com.starkloff.auth2.payload.response.MessageResponse;
import com.starkloff.auth2.payload.response.UserResponse;
import com.starkloff.auth2.services.AuthService;
import com.starkloff.auth2.services.PermissionService;

import com.starkloff.auth2.services.PrivilegedPermissionService;
import com.starkloff.auth2.services.PrivilegedService;
import org.apache.coyote.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/permission")
public class PermissionController extends ModelEntityController<PermissionDTO, PermissionService> {

    private final AuthService authService;
    private final PrivilegedService privilegedService;
    private final PrivilegedPermissionService privilegedPermissionService;

    public PermissionController(PermissionService entityService, AuthService authService, PrivilegedService privilegedService, PrivilegedPermissionService privilegedPermissionService) {
        super(entityService);
        this.authService = authService;
        this.privilegedService = privilegedService;
        this.privilegedPermissionService = privilegedPermissionService;
    }

    @PostMapping("/validate")
    public ResponseEntity<MessageResponse> validatePermissions(@RequestHeader("Authorization") String authorization, @RequestBody ValidatePermissionRequest validatePermissionRequest) throws Exception {
        UserResponse userResponse = this.authService.validateAction(authorization);
        for (String rol : userResponse.getPermissions()) {
            PrivilegedDTO privilegedDTO = this.privilegedService.getPrivilegedByIdPrivileged(rol);
            this.privilegedPermissionService.validatePermission(privilegedDTO, validatePermissionRequest);
        }
        PrivilegedDTO privilegedDTO = this.privilegedService.getPrivilegedByIdPrivileged(userResponse.getId());
        this.privilegedPermissionService.validateDeletedPermission(privilegedDTO, validatePermissionRequest);
        return ResponseEntity.ok(new MessageResponse("The user can perform the action"));
    }

    @GetMapping("/name/{name}")
    public ResponseEntity<PermissionDTO> getPermissionByName(@RequestHeader("Authorization") String authorization, @PathVariable String name) throws Exception {
        this.authService.validateAction(authorization);
        return ResponseEntity.ok(this.entityService.getPermissionByName(name));
    }
}
