package com.starkloff.auth2.dto;

import com.starkloff.auth2.common.dto.ModelEntityDTO;
import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PrivilegedPermissionDTO extends ModelEntityDTO {

    @NotNull(message = "Permission is required")
    PermissionDTO permission;

    @NotEmpty(message = "The user who assign the permission is required")
    private String idResponsible;

}
