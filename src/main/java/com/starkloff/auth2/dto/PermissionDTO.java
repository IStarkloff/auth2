package com.starkloff.auth2.dto;

import com.starkloff.auth2.common.dto.ModelEntityDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PermissionDTO extends ModelEntityDTO {
    @NotEmpty(message = "The permission must have a name")
    private String name;
}
