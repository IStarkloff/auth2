package com.starkloff.auth2.dto;

import com.starkloff.auth2.common.dto.ModelEntityDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PrivilegedDTO extends ModelEntityDTO {

    private List<PrivilegedPermissionDTO> privilegedPermissions;
    @NotEmpty(message = "User/role id is required ")
    private String idPrivileged;
}
