package com.starkloff.auth2.repositories;

import com.starkloff.auth2.common.repository.ModelEntityRepository;
import com.starkloff.auth2.models.Privileged;

import java.util.Optional;

public interface PrivilegedRepository extends ModelEntityRepository<Privileged> {

    Optional<Privileged> findByIdPrivileged(String id);
}
