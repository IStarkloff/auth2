package com.starkloff.auth2.repositories;

import com.starkloff.auth2.common.repository.ModelEntityRepository;
import com.starkloff.auth2.models.Permission;

import java.util.Optional;

public interface PermissionRepository extends ModelEntityRepository<Permission> {

    Optional<Permission> findByName(String name);


}
