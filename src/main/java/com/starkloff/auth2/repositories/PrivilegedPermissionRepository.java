package com.starkloff.auth2.repositories;

import com.starkloff.auth2.common.repository.ModelEntityRepository;
import com.starkloff.auth2.models.Permission;
import com.starkloff.auth2.models.Privileged;
import com.starkloff.auth2.models.PrivilegedPermission;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface PrivilegedPermissionRepository extends ModelEntityRepository<PrivilegedPermission> {

    @Query(value = "SELECT pp.* FROM privileged_permission pp WHERE pp.id_privileged = ?1 AND pp.id_permission = ?2", nativeQuery = true)
    Optional<PrivilegedPermission> findByPrivilegedIdAndPermissionId(Long privileged_id, Long permission_id);
}

