package com.starkloff.auth2.common.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Date;
import java.util.Optional;

@NoRepositoryBean
public interface ModelEntityRepository<T> extends JpaRepository<T, Long> {
    Page<T> findByDeletedDate(Date deletedDate, Pageable pageable);

    Optional<T> findByPrimaryKey(String primaryKey);
}
