package com.starkloff.auth2.common.mapper;

import com.starkloff.auth2.common.dto.ModelEntityDTO;
import com.starkloff.auth2.common.dto.PagedDTO;
import com.starkloff.auth2.exceptions.MapperException;
import com.starkloff.auth2.common.model.ModelEntity;
import com.starkloff.auth2.common.repository.ModelEntityRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.*;
import java.util.function.Function;

import static java.util.stream.Collectors.toList;

public abstract class EntityMapper<T extends ModelEntity, TDto extends ModelEntityDTO> {
    protected ModelEntityRepository<T> repository;
    protected Class<T> modelClass;
    protected Class<TDto> dtoClass;

    protected EntityMapper<? super T, ? super TDto> parentMapper;
    protected Collection<EntityMapper<? extends T, ? extends TDto>> childMappers;

    public EntityMapper(ModelEntityRepository<T> repository, Class<T> modelClass, Class<TDto> dtoClass) {
        this.repository = repository;
        this.modelClass = modelClass;
        this.dtoClass = dtoClass;
        this.childMappers = new ArrayList<>();
    }

    public EntityMapper(
            ModelEntityRepository<T> repository,
            Class<T> modelClass,
            Class<TDto> dtoClass,
            EntityMapper<? super T, ? super TDto> parentMapper
    ) {
        this(repository, modelClass, dtoClass);
        this.parentMapper = parentMapper;
        this.parentMapper.childMappers.add(this);
    }

    public abstract void mapToDto(T source, TDto destination, MappingContext context);

    public abstract void mapToModel(TDto source, T destination, MappingContext context);

    public TDto toDto(T source, MappingContext context) {
        if (source == null) {
            throw new MapperException("Nullable source in non-nullable toDto");
        }

        TDto destination = context.get(source, this.dtoClass);
        if (destination != null) {
            return destination;
        }

        if (parentMapper != null) {
            return (TDto) this.parentMapper.toDto(source, context);
        }

        try {
            Optional<EntityMapper> childMapper = getSpecificChildMapperForModelClass(source.getClass());
            if (childMapper.isPresent()) {
                destination = (TDto) childMapper.get().dtoClass.newInstance();
            } else {
                destination = this.dtoClass.newInstance();
            }
        } catch (InstantiationException | IllegalAccessException e) {
            throw new MapperException(
                    String.format("Couldn't instantiate the class %s when mapping to dto", this.dtoClass.getSimpleName()),
                    e
            );
        }

        context.put(source, this.dtoClass, destination);
        mapToDtoAndSubClasses(source, destination, context);
        return destination;
    }

    public T toModel(TDto source, MappingContext context) {
        if (source == null) {
            throw new MapperException("Nullable source in non-nullable toModel");
        }

        T destination = context.get(source, this.modelClass);
        if (destination != null) {
            return destination;
        }

        if (parentMapper != null) {
            return (T) this.parentMapper.toModel(source, context);
        }

        Optional<EntityMapper> childMapper = getSpecificChildMapperForDtoClass(source.getClass());

        try {
            Long existentId = source.getId();

            if (existentId != null) {
                destination =
                        (childMapper.isPresent())
                                ? (T) childMapper.get().repository.findById(existentId).get()
                                : this.repository.findById(existentId).get();
            } else {
                destination =
                        (childMapper.isPresent()) ? (T) childMapper.get().modelClass.newInstance() : this.modelClass.newInstance();
            }
        } catch (InstantiationException | IllegalAccessException e) {
            throw new MapperException(
                    String.format("Couldn't instantiate the class %s when mapping to model", this.modelClass.getSimpleName()),
                    e
            );
        }

        context.put(source, this.modelClass, destination);
        mapToModelAndSubClasses(source, destination, context);
        return destination;
    }

    public T toModelById(Long sourceId) {
        return (sourceId != null) ? this.repository.findById(sourceId).get() : null;
    }

    public List<TDto> mapCollectionToDtos(Collection<T> models, MappingContext context) {
        if (models == null) {
            return null;
        }

        List<TDto> dtos = new ArrayList<TDto>();
        for (T model : models) {
            dtos.add(this.toDto(model, context));
        }

        return dtos;
    }

    public List<T> mapCollectionToModels(Collection<TDto> dtos, MappingContext context) {
        if (dtos == null) {
            return null;
        }

        List<T> models = new ArrayList<T>();
        for (TDto dto : dtos) {
            models.add(this.toModel(dto, context));
        }
        return models;
    }

    public Set<TDto> mapCollectionToSetOfDtos(Collection<T> models, MappingContext context) {
        return new HashSet<TDto>(this.mapCollectionToDtos(models, context));
    }

    public List<Long> mapToIds(Collection<? extends ModelEntity> entities) {
        return entities.stream().map(entity -> entity.getId()).collect(toList());
    }

    public void mergeNestedCollection(
            Collection<T> destinationCollection,
            Collection<TDto> sourceCollection,
            MappingContext context
    ) {
        this.mergeCollection(destinationCollection, sourceCollection, context);
        this.replaceOldEntityByEdited(destinationCollection, sourceCollection, context);
    }

    public void mergeCollection(
            Collection<T> destinationCollection,
            Collection<TDto> sourceCollection,
            MappingContext context
    ) {
        List<Long> sourceIds = sourceCollection.stream().map(dto -> dto.getId()).filter(id -> id != null).collect(toList());
        this.mergeCollectionWithIds(destinationCollection, sourceIds);
        this.addNewEntitiesToDestination(destinationCollection, sourceCollection, context);
    }

    private void addNewEntitiesToDestination(
            Collection<T> destinationCollection,
            Collection<TDto> sourceCollection,
            MappingContext context
    ) {
        List<TDto> dtosOfNewEntities = sourceCollection.stream().filter(dto -> dto.getId() == null).collect(toList());
        dtosOfNewEntities.forEach(dto -> destinationCollection.add(this.toModel(dto, context)));
    }

    public void mergeCollectionWithIds(Collection<T> destinationCollection, Collection<Long> sourceCollectionIds) {
        this.removeDeletedEntitiesFromDestination(destinationCollection, sourceCollectionIds);
        this.addExistentEntitiesToDestination(destinationCollection, sourceCollectionIds);
    }

    private void removeDeletedEntitiesFromDestination(
            Collection<T> destinationCollection,
            Collection<Long> sourceCollectionIds
    ) {
        destinationCollection.removeIf(element -> !sourceCollectionIds.contains(element.getId()));
    }

    private void addExistentEntitiesToDestination(
            Collection<T> destinationCollection,
            Collection<Long> sourceCollectionIds
    ) {
        for (Long sourceCollectionId : sourceCollectionIds) {
            if (!destinationCollection.stream().anyMatch(i -> i.getId().equals(sourceCollectionId))) {
                destinationCollection.add(this.repository.findById(sourceCollectionId).get());
            }
        }
    }

    private void replaceOldEntityByEdited(
            Collection<T> destinationCollection,
            Collection<TDto> sourceCollection,
            MappingContext context
    ) {
        for (TDto source : sourceCollection) {
            if (destinationCollection.stream().anyMatch(i -> i.getId() == source.getId())) {
                destinationCollection.remove(destinationCollection.stream().anyMatch(i -> i.getId() == source.getId()));
                destinationCollection.add(this.toModel(source, context));
            }
        }
    }

    public PagedDTO<TDto> mapPagedCollectionToPagedDtos(Page<T> models, MappingContext context) {
        return new PagedDTO<TDto>(
                (int) models.getTotalElements(),
                models.getTotalPages(),
                this.mapCollectionToDtos(models.getContent(), context)
        );
    }

    public void mergeElementCollection(Collection<String> destinationCollection, Collection<String> sourceCollection) {
        destinationCollection.removeIf(element -> !sourceCollection.contains(element));

        for (String sourceValue : sourceCollection) {
            if (!destinationCollection.contains(sourceValue)) {
                destinationCollection.add(sourceValue);
            }
        }
    }

    public List<String> parseStringToList(String stringToParse, String separator) {
        if (stringToParse == null || stringToParse.length() == 0) {
            return new ArrayList<String>();
        }
        return new ArrayList<String>(Arrays.asList(stringToParse.split(separator)));
    }

    public String stringsListToString(List<String> list, String separator) {
        return list == null ? "" : String.join(separator, list);
    }

    public T nullableEntityToModel(TDto source, MappingContext context) {
        if (source == null) {
            return null;
        }

        return this.toModel(source, context);
    }

    public TDto nullableEntityToDto(T source, MappingContext context) {
        if (source == null) {
            return null;
        }

        return this.toDto(source, context);
    }

    public Page<T> completeEntities(Page<T> incompleteEntities, Function<List<Long>, List<T>> completionFunction) {
        Long entitiesTotalCount = incompleteEntities.getTotalElements();
        List<Long> filteredIds = incompleteEntities.getContent().stream().map(ModelEntity::getId).collect(toList());
        List<T> completeEntities = completionFunction.apply(filteredIds);

        if (completeEntities.size() > 1) {
            completeEntities = this.reorderEntities(completeEntities, filteredIds);
        }

        return new PageImpl<T>(completeEntities, incompleteEntities.getPageable(), entitiesTotalCount);
    }

    private List<T> reorderEntities(List<T> unorderedEntites, List<Long> orderedEntitiesIds) {
        List<T> orderedEntities = new ArrayList<T>();
        orderedEntitiesIds.forEach(
                id -> {
                    T nextEntity = unorderedEntites.stream().filter(entity -> entity.getId().equals(id)).findFirst().get();
                    orderedEntities.add(nextEntity);
                }
        );
        return orderedEntities;
    }

    public ModelEntityRepository<T> getRepository() {
        return this.repository;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public void mapToDtoAndSubClasses(T source, TDto destination, MappingContext context) {
        this.mapToDto(source, destination, context);

        this.getChildMapperForModelClass(source.getClass())
                .ifPresent(
                        mapper -> {
                            mapper.mapToDtoAndSubClasses(source, destination, context);
                        }
                );
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private Optional<EntityMapper> getSpecificChildMapperForModelClass(Class<? extends ModelEntity> modelClass) {
        Optional<EntityMapper> childMapper = getChildMapperForModelClass(modelClass);

        if (!childMapper.isPresent()) {
            return Optional.empty();
        }
        if (childMapper.get().modelClass == modelClass) {
            return childMapper;
        }
        return childMapper.get().getSpecificChildMapperForModelClass(modelClass);
    }

    private Optional<EntityMapper> getChildMapperForModelClass(Class<? extends ModelEntity> modelClass) {
        for (EntityMapper<? extends T, ? extends TDto> childMapper : this.childMappers) {
            if (childMapper.modelClass.isAssignableFrom(modelClass)) {
                return Optional.of(childMapper);
            }
        }
        return Optional.empty();
    }

    @SuppressWarnings({"unchecked"})
    public void mapToModelAndSubClasses(TDto source, T destination, MappingContext context) {
        this.mapToModel(source, destination, context);

        this.getChildMapperForDtoClass(source.getClass())
                .ifPresent(
                        mapper -> {
                            mapper.mapToModelAndSubClasses(source, destination, context);
                        }
                );
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private Optional<EntityMapper> getSpecificChildMapperForDtoClass(Class<? extends ModelEntityDTO> dtoClass) {
        Optional<EntityMapper> childMapper = getChildMapperForDtoClass(dtoClass);

        if (!childMapper.isPresent()) {
            return Optional.empty();
        }
        if (childMapper.get().dtoClass == dtoClass) {
            return childMapper;
        }
        return childMapper.get().getSpecificChildMapperForDtoClass(dtoClass);
    }

    private Optional<EntityMapper> getChildMapperForDtoClass(Class<? extends ModelEntityDTO> dtoClass) {
        for (EntityMapper<? extends T, ? extends TDto> childMapper : this.childMappers) {
            if (childMapper.dtoClass.isAssignableFrom(dtoClass)) {
                return Optional.of(childMapper);
            }
        }
        return Optional.empty();
    }
}
