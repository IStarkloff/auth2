package com.starkloff.auth2.common.mapper;

import java.util.HashMap;
import java.util.Map;

public class MappingContext {
    private final Map<MappingKey, Object> mappings;

    public MappingContext() {
        this.mappings = new HashMap<>();
    }

    @SuppressWarnings("unchecked")
    public <TSource, TDestination> TDestination get(TSource source, Class<? extends TDestination> destinationType) {
        return (TDestination) this.mappings.get(new MappingKey(source, destinationType));
    }

    public <TSource, TDestination> void put(
            TSource source,
            Class<? extends TDestination> destinationType,
            TDestination destination
    ) {
        this.mappings.put(new MappingKey(source, destinationType), destination);
    }

    private static class MappingKey {
        private final Object source;
        private final Class<?> destinationType;

        MappingKey(Object source, Class<?> destinationType) {
            if (source == null) {
                throw new IllegalArgumentException("Mapping source cannot be null");
            }
            if (destinationType == null) {
                throw new IllegalArgumentException("Mapping destination type cannot be null");
            }
            this.source = source;
            this.destinationType = destinationType;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + this.destinationType.hashCode();
            result = prime * result + this.source.hashCode();
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }

            if (obj == null) {
                return false;
            }

            if (this.getClass() != obj.getClass()) {
                return false;
            }

            MappingKey other = (MappingKey) obj;
            if (this.destinationType != other.destinationType) {
                return false;
            }

            return this.source.equals(other.source);
        }
    }
}
