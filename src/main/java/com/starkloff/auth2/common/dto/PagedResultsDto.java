package com.starkloff.auth2.common.dto;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PagedResultsDto<TDto> {
    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "frontendType")
    private List<TDto> entities;

    private Long totalEntitiesCount;
    private Long totalPagesCount;

    public PagedResultsDto(Page<TDto> page) {
        this.entities = page.getContent();
        this.totalEntitiesCount = page.getTotalElements();
        this.totalPagesCount = Long.valueOf(page.getTotalPages());
    }
}
