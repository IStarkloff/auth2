package com.starkloff.auth2.common.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PagedDTO<T extends ModelEntityDTO> {
    private int total;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer page;
    private List<T> items;
}
