package com.starkloff.auth2.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ModelEntityDTO {
    private Long id;
    private Date deletedDate;
    private Date createdDate = new Date();
}
