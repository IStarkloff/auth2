package com.starkloff.auth2.common.service;

import com.starkloff.auth2.common.dto.ModelEntityDTO;
import com.starkloff.auth2.common.dto.PagedDTO;
import org.springframework.data.domain.Pageable;

public interface ModelEntityService<T extends ModelEntityDTO> {

    PagedDTO<T> getAll(Pageable pageable);

    PagedDTO<T> getAllActive(Pageable pageable);

    T get(Long id);

    T create(T dto);

    void delete(Long id);

    T update(T dto);
}
