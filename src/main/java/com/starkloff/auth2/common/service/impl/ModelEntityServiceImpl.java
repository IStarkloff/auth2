package com.starkloff.auth2.common.service.impl;

import com.starkloff.auth2.common.dto.ModelEntityDTO;
import com.starkloff.auth2.common.dto.PagedDTO;
import com.starkloff.auth2.common.mapper.EntityMapper;
import com.starkloff.auth2.common.mapper.MappingContext;
import com.starkloff.auth2.common.model.ModelEntity;
import com.starkloff.auth2.common.repository.ModelEntityRepository;
import com.starkloff.auth2.common.service.ModelEntityService;
import com.starkloff.auth2.exceptions.AlreadyExistException;
import com.starkloff.auth2.exceptions.ServiceException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public abstract class ModelEntityServiceImpl<T extends ModelEntity, TDto extends ModelEntityDTO, TRepository extends ModelEntityRepository<T>, TMapper extends EntityMapper<T, TDto>>
        implements ModelEntityService<TDto> {
    protected TRepository repository;
    protected TMapper mapper;
    protected Class<T> entityClass;
    protected Class<TDto> dtoClass;

    public ModelEntityServiceImpl(Class<T> entityClass, Class<TDto> dtoClass, TRepository repository, TMapper mapper) {
        this.entityClass = entityClass;
        this.dtoClass = dtoClass;
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public PagedDTO<TDto> getAll(Pageable pageable) {
        Page<T> entities = repository.findAll(pageable);
        return mapper.mapPagedCollectionToPagedDtos(entities, new MappingContext());
    }

    @Override
    public PagedDTO<TDto> getAllActive(Pageable pageable) {
        Page<T> entities = this.repository.findByDeletedDate(null, pageable);
        return this.mapper.mapPagedCollectionToPagedDtos(entities, new MappingContext());
    }

    @Override
    public TDto get(Long id) {
        try {
            Optional<T> result = repository.findById(id);
            if (result.isPresent()) {
                return mapper.toDto(result.get(), new MappingContext());
            }
            throw new NoSuchElementException(this.dtoClass.getSimpleName() + " was not found with id " + id);
        } catch (NoSuchElementException ne) {
            throw new NoSuchElementException(ne.getMessage());
        } catch (Exception e) {
            throw new ServiceException(
                    String.format("Couldn't instantiate the %s when getting by id", this.dtoClass.getSimpleName())
            );
        }
    }


    @Override
    public TDto create(TDto dto) {
        try {
            T entityToSave = mapper.toModel(dto, new MappingContext());
            Optional<T> existentEntity = repository.findByPrimaryKey(entityToSave.getPrimaryKey());
            if (existentEntity.isPresent()) {
                throw new AlreadyExistException("There is already one " + this.entityClass.getSimpleName() + " with the " + existentEntity.get().getNamePrimaryKey() + " entered");
            }
            return this.save(entityToSave);
        } catch (AlreadyExistException e) {
            throw new AlreadyExistException(e.getMessage());
        } catch (Exception e) {
            throw new ServiceException(
                    String.format("Couldn't instantiate the %s when creating a new entity", this.entityClass.getSimpleName()),
                    e
            );
        }
    }

    @Override
    public void delete(Long id) {
        try {
            Optional<T> optT = this.repository.findById(id);
            if (optT.isPresent()) {
                optT.get().setDeletedDate(new Date());
                this.repository.save(optT.get());
            }
        } catch (Exception e) {
            throw new ServiceException(
                    String.format("Couldn't instantiate the %s when erasing a entity", this.dtoClass.getSimpleName())
            );
        }
    }

    @Override
    public TDto update(TDto dto) {
        try {
            Optional<T> entityToUpdate = repository.findById(dto.getId());
            if (entityToUpdate.isEmpty()) {
                throw new NoSuchElementException(
                        String.format(
                                "Couldn't update %s with id: %s because it was not present",
                                this.dtoClass.getSimpleName(),
                                dto.getId()
                        )
                );
            }
            T entityToSave = entityToUpdate.get();
            String primaryKey = entityToUpdate.get().getPrimaryKey();
            mapper.mapToModel(dto, entityToSave, new MappingContext());
            if (!entityToSave.getPrimaryKey().equals(primaryKey)) {
                Optional<T> existentEntity = repository.findByPrimaryKey(entityToSave.getPrimaryKey());
                if (existentEntity.isPresent()) {
                    throw new AlreadyExistException("There is already one " + this.entityClass.getSimpleName() + " with the " + existentEntity.get().getNamePrimaryKey() + " entered");
                }
            }
            return this.save(entityToSave);
        } catch (NoSuchElementException ne) {
            throw new NoSuchElementException(ne.getMessage());
        } catch (AlreadyExistException ne) {
            throw new AlreadyExistException(ne.getMessage());
        } catch (Exception e) {
            throw new ServiceException(
                    String.format("Couldn't instantiate the %s when getting by id", this.dtoClass.getSimpleName())
            );
        }
    }

    protected TDto save(T entityToSave) {
        try {
            T savedEntity = repository.save(entityToSave);
            return mapper.toDto(savedEntity, new MappingContext());
        } catch (Exception e) {
            throw new ServiceException(
                    String.format("Couldn't instantiate the %s when saving", this.entityClass.getSimpleName()),
                    e
            );
        }
    }
}
