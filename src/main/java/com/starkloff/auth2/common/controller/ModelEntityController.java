package com.starkloff.auth2.common.controller;

import com.starkloff.auth2.common.dto.ModelEntityDTO;
import com.starkloff.auth2.common.dto.PagedDTO;
import com.starkloff.auth2.common.service.ModelEntityService;
import com.starkloff.auth2.services.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

public abstract class ModelEntityController<TDto extends ModelEntityDTO, E extends ModelEntityService<TDto>> {
    protected E entityService;
    @Autowired
    private AuthService authService;

    public ModelEntityController(E entityService) {
        this.entityService = entityService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<TDto> getById(@RequestHeader("Authorization") String authorization, @PathVariable("id") Long id) throws Exception {
        this.authService.validateAction(authorization);
        return ResponseEntity.ok(this.entityService.get(id));
    }

    @GetMapping("/all")
    public ResponseEntity<PagedDTO<TDto>> getAll(@RequestHeader("Authorization") String authorization, Pageable pageable) throws Exception {
        this.authService.validateAction(authorization);
        return ResponseEntity.ok(this.entityService.getAll(pageable));
    }

    @GetMapping
    public ResponseEntity<PagedDTO<TDto>> getAllActive(@RequestHeader("Authorization") String authorization, Pageable pageable) throws Exception {
        this.authService.validateAction(authorization);
        return ResponseEntity.ok(this.entityService.getAllActive(pageable));
    }

    @PostMapping
    public ResponseEntity<TDto> create(@RequestHeader("Authorization") String authorization, @RequestBody TDto newEntity) throws Exception {
        this.authService.validateActionAdmin(authorization);
        return ResponseEntity.status(201).body(this.entityService.create(newEntity));
    }

    @PutMapping
    public ResponseEntity<TDto> update(@RequestHeader("Authorization") String authorization, @RequestBody TDto entityToUpdate) throws Exception {
        this.authService.validateActionAdmin(authorization);
        return ResponseEntity.ok(this.entityService.update(entityToUpdate));
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader("Authorization") String authorization, @PathVariable("id") Long id) throws Exception {
        this.authService.validateActionAdmin(authorization);
        this.entityService.delete(id);
    }
}
