package com.starkloff.auth2.exceptions;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class NotAcceptableException extends RuntimeException {
    public NotAcceptableException(String errorMessage) {
        super(errorMessage);
    }
}
