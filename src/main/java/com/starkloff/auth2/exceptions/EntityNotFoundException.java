package com.starkloff.auth2.exceptions;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class EntityNotFoundException extends ServiceException {
    private static final long serialVersionUID = -3610030100458199131L;

    public EntityNotFoundException(String message) {
        super(message);
    }

    public EntityNotFoundException(String message, Exception innerException) {
        super(message, innerException);
    }
}
