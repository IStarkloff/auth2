package com.starkloff.auth2.exceptions;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class MapperException extends RuntimeException {
    private static final long serialVersionUID = -185781350206562176L;

    public MapperException(String message) {
        super(message);
    }

    public MapperException(String message, Exception innerException) {
        super(message, innerException);
    }
}
