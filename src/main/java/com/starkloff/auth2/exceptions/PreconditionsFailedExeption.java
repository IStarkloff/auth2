package com.starkloff.auth2.exceptions;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class PreconditionsFailedExeption extends RuntimeException {
    public PreconditionsFailedExeption(String errorMessage) {
        super(errorMessage);
    }
}
