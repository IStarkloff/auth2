package com.starkloff.auth2.exceptions;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class ServiceException extends RuntimeException {
    private static final long serialVersionUID = 3771821614454631372L;

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Exception innerException) {
        super(message, innerException);
    }
}
