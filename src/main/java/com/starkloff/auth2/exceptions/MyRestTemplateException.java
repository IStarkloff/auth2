package com.starkloff.auth2.exceptions;

import com.starkloff.auth2.errors.ApiError;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MyRestTemplateException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private ApiError apiError;

    public MyRestTemplateException(String errorMessage, ApiError apiError) {
        super(errorMessage);
        this.apiError = apiError;
    }


}
