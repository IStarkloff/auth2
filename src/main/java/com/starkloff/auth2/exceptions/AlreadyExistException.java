package com.starkloff.auth2.exceptions;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class AlreadyExistException extends RuntimeException {
    public AlreadyExistException(String errorMessage) {
        super(errorMessage);
    }
}
