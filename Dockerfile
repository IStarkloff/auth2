FROM maven:3.6.0-jdk-13-alpine

LABEL maintainer="starkloffnacho@gmail.com"

WORKDIR /auth2

COPY ./pom.xml ./pom.xml

COPY ./src ./src

RUN mvn dependency:go-offline

CMD ["mvn", "clean", "spring-boot:run"]
